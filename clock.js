function counting() {
    var today =  new Date();

    var day = today.getDate();
    var month = today.getMonth()+1;
    var year = today.getFullYear();

    var sec = today.getSeconds();
    if(sec<10) sec = "0"+sec;
    var min = today.getMinutes();
    if(min<10) min = "0"+min;
    var hr = today.getHours();
    if(hr<10) hr = "0"+hr;

    var data = document.getElementById("data");
    var czas = document.getElementById("czas");
    data.innerHTML = day+"/"+month+"/"+year;
    czas.innerHTML = hr+":"+min+":"+sec;

    setTimeout(function(){counting();}, 1000);
}